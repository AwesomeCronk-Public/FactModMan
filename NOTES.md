# Next to implement

1. Command to check if factmand is running
2. Fix help strings for subcommands
3. Way to add multiple mods at once
4. Autostart when factmand starts, move autostart logic to factmand
5. Check saves before starting inst to see if any autosaves are newer than the main save
6. Web page showing live status, addresses, etc
7. Mod dependency resolution
8. Mod settings editor
9. ntfy support


```
aweso@PowerEdgeR720:~$ ls /opt/factorio
achievements.dat  config-path.cfg       factorio-previous.log  saves                  temp
bin               data                  mods                   server-adminlist.json
config            factorio-current.log  player-data.json       server-whitelist.json
```


Unix socket errors:

- Connecting to socket that doesn't exist: FileNotFoundError
- Connecting to socket that's busy: no error, will need timeout

You will need the `tar` command available to extract headless install archives

When extracting headless files, if tar -xf fails then it's possible that factorio.com served a .tar.gz archive instead of .tar.xz. It shouldn't make a difference, but FactMan assumes that it's .tar.xz and uses that file extension, which *might* cause tar to lose its shit. Idk I prefer .7z and don't work with .tar.xz by choice.

# mod-settings.dat

### File structure:

- uint16le: version main
- uint16le: version major
- uint16le: version minor
- uint16le: version patch
- uint8: version branch
- ptree: property tree

### ptree structure

- uint8: property tree type
- uint8: is any type (discard)
- if type == 0: none
    - no value follows
- if type == 1: bool
    - uint8: value (true if != 0)
- if type == 2: number
    - doublele: value
- if type == 3: string
    - ptreestring: value
- if type == 4: list
    - uint32le: count
    - (count) times:
        - ptreestring: key (empty, discard)
        - ptree: value
- if type == 5: dictionary
    - uint32le: count
    - (count) times:
        - ptreestring: key
        - ptree: value
- if type == 6: signedinteger
    - int64le: value
- if type == 7: unsignedinteger
    - unit64le: value

### ptreestring structure

- uint8: empty
- if empty != 0: true
    - no value follows
- if empty == 0: false
    - packeduint_8_32: size
    - string: value

### packeduint_8_32 structure

- uint8: size
- if size == 0xff:
    uint32le: value
- else:
    size is value

