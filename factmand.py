#!/usr/bin/env python3

import json, os, socket, subprocess, sys, threading, traceback, time
from datetime import datetime, timedelta
from time import sleep  # , perf_counter_ns

from utils import *


class _flags: pass
flags = _flags()

flags.run = True
flags.loadInsts = False
flags.crashMainThread = False


insts = {}

# Inst structure:
# {
#     'automatic': True|False,
#     'status': 'running'|'stopping'|'stopped'|'crashed out',
#     'action': 'start'|'stop'|'get players'|'',
#     'lastResult': 'could be many things',
#     'proc': subprocess.Popen|None,
#     'crashes': [datetime.datetime,...],
#     'stdout': '',
#     'stderr': ''
# }


def _commandThread():
    print('C:Setting up monitor socket')

    # Set up socket
    paths.factmand_socket.unlink(missing_ok=True)  # Make sure the socket path is available
    monitorSocket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    monitorSocket.settimeout(1)    # seconds
    monitorSocket.bind(str(paths.factmand_socket.resolve()))
    monitorSocket.listen(1)

    print('C:Monitor socket set up and listening')

    while flags.run:
        # Get a connection, try again if timeout
        try: conn, addr = monitorSocket.accept()
        except socket.timeout: continue
        print('C:Frontend connected')
        
        try:
            # Receive and decode request
            reqRaw = conn.recv(1024)
            try: req = json.loads(reqRaw.decode())
            except UnicodeDecodeError or json.JSONDecodeError:
                print('C:Failed to decode request: {}'.format(reqRaw))
                conn.send(json.dumps({'result': 'err', 'reason': 'bad request'}))
                continue
            
            print('C:Received request', json.dumps(req))

            # Get cmd
            try: cmd = req['cmd']
            except KeyError:
                conn.send(json.dumps({'result': 'err', 'reason': 'expected key "cmd"'}).encode())
                continue

            # Print debug information
            if cmd == 'dbg':
                print('C:dbg flags.run: ', flags.run)
                print('C:dbg flags.reloadInsts: ', flags.reloadInsts)
                print('C:dbg mainLoopNum: ', mainLoopNum)
                conn.send(json.dumps({'result': 'ok'}).encode())

            # Start or stop an instance
            # 'restart' should be achieved by sending 'stop' then 'start'
            elif cmd in ('start', 'stop'):
                # Get instName
                try: instName = req['inst']
                except KeyError:
                    conn.send(json.dumps({'result': 'err', 'reason': 'expected key "inst"'}))
                    continue
                if not instName in insts.keys():
                    conn.send(json.dumps({'result': 'err', 'reason': 'bad inst name'}).encode())
                    continue
                
                # Set the action
                insts[instName]['action'] = cmd
                print('C:Set {} action to {}'.format(instName, cmd))

                # Wait up to 1 second for main thread to take action
                for i in range(10):
                    if insts[instName]['action'] == '':
                        conn.send(json.dumps(insts[instName]['lastResult']).encode())   # TODO: Make this conform to {'result': 'ok'} standard
                        break
                    sleep(0.1)
                else: conn.send(json.dumps({'result': 'err', 'reason': 'main thread timeout'}).encode())

            elif cmd == 'autostart':
                instsStarted = []
                # Iterate all insts
                for instName in insts.keys():
                    inst = insts[instName]

                    # Check if inst can be autostarted
                    if inst['automatic'] and inst['status'] == 'stopped':

                        # Set the action
                        inst['action'] = 'start'
                        print('C:Set {} action to start'.format(instName))

                        # Wait up to 1 second for main thread to take action
                        for i in range(10):
                            # Since we check inst status before setting inst action to start
                            # and the only way for inst lastResult to be err is if inst wasn't
                            # stopped before, only failure mode to catch right now is main
                            # thread timeout. Inst action == '' indicates main thread took
                            # action and did not time out.
                            if inst['action'] == '':
                                instsStarted.append(instName)
                                break
                            sleep(0.1)
                        else:
                            conn.send(json.dumps({'result': 'err', 'reason': 'main thread timeout'}).encode())
                            break   # If the main thread is not responding don't keep trying

                # Send results
                if len(instsStarted):
                    conn.send(json.dumps({'result': 'ok', 'started': instsStarted}).encode())
                else:
                    conn.send(json.dumps({'result': 'err', 'reason': 'no insts to start'}).encode())
                
                
            # Reload insts listing
            elif cmd == 'reload':
                flags.loadInsts = True

                # Wait up to 1 second for main thread to take action
                for i in range(10):
                    if not flags.loadInsts:
                        conn.send(json.dumps({'result': 'ok'}).encode())
                        break
                    sleep(0.1)
                else: conn.send(json.dumps({'result': 'err', 'reason': 'main thread timeout'}).encode())

            # Get inst status
            elif cmd == 'status':
                try: instName = req['inst']
                except KeyError:
                    conn.send(json.dumps({'result': 'err', 'reason': 'expected key "inst"'}))
                    continue
                if not instName in insts.keys():
                    conn.send(json.dumps({'result': 'err', 'reason': 'bad inst name'}).encode())
                    continue
                
                conn.send(json.dumps({'result': 'ok', 'status': insts[instName]['status']}).encode())

            # Get length of inst's stdout
            elif cmd == 'stdoutLen':
                try: instName = req['inst']
                except KeyError:
                    conn.send(json.dumps({'result': 'err', 'reason': 'expected key "inst"'}))
                    continue
                if not instName in insts.keys():
                    conn.send(json.dumps({'result': 'err', 'reason': 'bad inst name'}).encode())
                    continue
                
                conn.send(json.dumps({'result': 'ok', 'stdout': len(insts[instName]['stdout'])}).encode())

            # Get length of inst's stdout as encoded in JSON
            elif cmd == 'stdoutJSONLen':
                try: instName = req['inst']
                except KeyError:
                    conn.send(json.dumps({'result': 'err', 'reason': 'expected key "inst"'}))
                    continue
                if not instName in insts.keys():
                    conn.send(json.dumps({'result': 'err', 'reason': 'bad inst name'}).encode())
                    continue
                
                conn.send(json.dumps({'result': 'ok', 'stdout': len(json.dumps(insts[instName]['stdout']))}).encode())

            # Get inst's stdout
            elif cmd == 'stdout':
                if req['inst'] not in insts.keys():
                    conn.send(json.dumps({'result': 'err', 'reason': 'bad inst name'}).encode())
                
                resp = json.dumps({'result': 'ok', 'stdout': insts[req['inst']]['stdout']}).encode()
                print('C:Sending stdout resp (len {})'.format(len(resp)))
                conn.send(resp)

            # Get list of online players
            elif cmd == 'players':
                try: instName = req['inst']
                except KeyError:
                    conn.send(json.dumps({'result': 'err', 'reason': 'expected key "inst"'}))
                    continue
                if not instName in insts.keys():
                    conn.send(json.dumps({'result': 'err', 'reason': 'bad inst name'}).encode())
                    continue
                
                inst = insts[instName]

                inst['action'] = 'get players'
                print('C:Set {} action to get players'.format(instName))

                # Wait up to 1 second for main thread to take action
                for i in range(10):
                    if insts[instName]['action'] == '':
                        conn.send(json.dumps(insts[instName]['lastResult']).encode())   # TODO: Make this conform to {'result': 'ok'} standard
                        break
                    sleep(0.1)
                else: conn.send(json.dumps({'result': 'err', 'reason': 'main thread timeout'}).encode())




            ### THESE COMMANDS ARE FOR DEBUGGING PURPOSES ###

            # Crash the command thread
            elif cmd == 'crashcmd':
                conn.send(json.dumps({'result': 'ok'}).encode())
                raise Exception('crashcmd receieved')

            # Crash the main thread
            elif cmd == 'crashmain':
                conn.send(json.dumps({'result': 'ok'}).encode())
                flags.crashMainThread = True

            # Invalid command
            else:
                print('C:Bad command')
                conn.send(json.dumps({'result': 'err', 'reason': 'bad command'}).encode())

        # Housekeeping if command thread crashes
        except:
            crashMsg = '\nC:Error in command thread:\n{}'.format(traceback.format_exc())
            print(crashMsg)
            with open(paths.factmand_crashes, 'a') as crashFile:
                crashFile.write(datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ': ' + crashMsg + '\n\n')

            print('C:Setting flags.run to False')
            flags.run = False

            print('C:Breaking loop')
            break

        # Housekeeping for each connection
        finally:
            print('C:Closing frontend connection')
            conn.close()
        
    print('C:Command thread exiting')


def startProc(instName):
    print('M:Starting inst {}'.format(instName))

    instDir = paths.instances.joinpath(instName)

    # Load inst datafile
    # TODO: Sanitize it
    with open(instDir.joinpath('factman.json'), 'r') as instDatafile:
        instData = json.load(instDatafile)
    
    # Clear stdout record
    insts[instName]['stdout'] = ''

    # Set up command line args
    cmd = [
        paths.headlessExec,
        '--start-server', instDir.joinpath('saves', '{}.zip'.format(instName)),
        '--config', instDir.joinpath('config', 'config.ini'),
        '--mod-directory', instDir.joinpath('mods'),
        '--port', str(instData['port'])
    ]
    
    if instData['use-whitelist']:
        cmd += ['--use-server-whitelist', '--server-whitelist', instDir.joinpath('server-whitelist.json')]

    if instData['use-banlist']:
        cmd += ['--server-banlist', instDir.joinpath('server-banlist.json')]

    if instData['use-adminlist']:
        cmd += ['--server-adminlist', instDir.joinpath('server-adminlist.json')]

    cmd += instData['cli-args']

    # Start process
    print('M:' + ' '.join([str(piece) for piece in cmd]))
    proc = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    # Set stdout/stderr to non-blocking
    os.set_blocking(proc.stdout.fileno(), False)
    
    return proc

def stopProc(instName):
    print('M:Stopping inst {}'.format(instName))
    
    if insts[instName]['proc'] is None:
        print('M:Proc is None')
        return
    
    insts[instName]['proc'].terminate()    # SIGTERM, seems to work fine


def loadInsts():
    print('M:Loading insts')
    # The autostart listing is made from a current directory listing, guaranteed up to date
    autostartListing = loadInstsListing()
    for instName in autostartListing.keys():
        if not instName in insts.keys():
            insts[instName] = {
                'status': 'stopped',
                'action': '',
                'lastResult': {},
                'proc': None,
                'crashes': [],
                'stdout': ''   # ,
                # 'stderr': ''
            }
        insts[instName]['automatic'] = autostartListing[instName]['automatic']

    # Check that we didn't have any removed
    for instName in insts.keys():
        if not instName in autostartListing.keys():
            # TODO: debug message
            print('M:Removing {} from insts'.format(instName))
            stopProc(instName)
            del insts[instName]


if __name__ == '__main__':
    print('M:factmand starting')

    # Check Python version
    if sys.version_info[0] < 3 or sys.version_info[1] < 5:
        print('M:This script requires Python 3.5 or higher')

    # Start command thread
    print('M:Starting command thread')
    commandThread = threading.Thread(target=_commandThread)
    commandThread.start()
    print('M:Command thread running')

    flags.loadInsts = True
    mainLoopNum = 0
    loopTimes = []

    try:
        print('M:Entering main loop')
        while flags.run:
            # loopBeginTime = perf_counter_ns()

            # Crash the main thread
            if flags.crashMainThread:
                raise Exception('crashmain received')

            # Reload inst listing
            if flags.loadInsts:
                loadInsts()
                flags.loadInsts = False
                print('M:Reloaded insts: {}'.format(insts))

            # Communicate with inst procs
            for instName in insts.keys():
                inst = insts[instName]
                instProc = inst['proc']

                # This should never happen unless there's an issue with factmand itself
                if instProc == None and inst['status'] == 'running':
                    print('M:No process for {} but status is running'.format())
                    inst['status'] = 'stopped'

                if instProc != None:
                    # Read and log stdout/stderr
                    stdoutRaw = instProc.stdout.read()
                    if not stdoutRaw in (None, b''):
                        print('M:Inst {} stdout update ({} bytes)'.format(instName, len(stdoutRaw)))
                        inst['stdout'] = inst['stdout'] + stdoutRaw.decode(errors='replace')

                    # Check for crashes
                    if instProc.poll() != None and inst['status'] == 'running':
                        # Record the crash time, only keep 3, log all of them
                        inst['crashes'].append(datetime.now())

                        # 3 crashes in 10 minutes?
                        if len(inst['crashes']) >= 3:
                            if (inst['crashes'][-1] - inst['crashes'][-3]) <= timedelta(minutes=10):
                                print('M:Inst {} crashed out'.format(instName))
                                inst['status'] = 'crashed out'
                        
                        # Restart it
                        else:
                            print('M:Inst {} crashed, restarting'.format(instName))
                            del inst['proc']    # Unneccessary?
                            newInstProc = startProc(instName)
                            inst['proc'] = newInstProc

                # Start an inst
                if inst['action'] == 'start':
                    print('M:Should start {}'.format(instName))
                    
                    # Can only start if it's previously stopped
                    if inst['status'] != 'stopped':
                        inst['lastResult'] = {'result': 'err', 'reason': {'status': inst['status']}}
                    
                    newInstProc = startProc(instName)
                    inst['proc'] = newInstProc
                    inst['status'] = 'running'
                    inst['lastResult'] = {'result': 'ok'}

                # Stop an inst
                elif inst['action'] == 'stop':
                    print('M:Should stop {}'.format(instName))
                    if inst['status'] != 'stopped':
                        inst['status'] = 'stopping'
                        stopProc(instName)
                        inst['status'] = 'stopped'
                        inst['lastResult'] = {'result': 'ok'}
                    else:
                        inst['lastResult'] = {'result': 'err', 'reason': {'status': inst['status']}}

                # Get list of online players
                elif inst['action'] == 'get players':
                    if inst['status'] != 'running':
                        inst['lastResult'] = {'result': 'err', 'reason': {'status': inst['status']}}

                    else:
                        print('M:Sending "/players online" to {} stdin'.format(instName))
                        inst['proc'].stdin.write(b'/players online\n')
                        inst['proc'].stdin.flush()

                        # Try to read until data is available
                        stdoutRaw = None
                        while stdoutRaw in (None, b''):
                            stdoutRaw = inst['proc'].stdout.read()

                        # Keep reading data until no data is available
                        stdoutRawFresh = inst['proc'].stdout.read()
                        while not stdoutRawFresh in (None, b''):
                            stdoutRaw = stdoutRaw + stdoutRawFresh
                            stdoutRawFresh = inst['proc'].stdout.read()
                        
                        # Split data by lines and look for "Online players"
                        stdout = stdoutRaw.decode(errors='replace')
                        stdoutLines = [line.strip() for line in stdout.split('\n')]
                        for l, line in enumerate(stdoutLines):
                            if line[0:14] == 'Online players': break
                        else: l = -1
                        
                        # Get the number after that and grab names on that many more lines
                        players = []
                        if l != -1:
                            numPlayers = int(stdoutLines[l][16:].split(')')[0])    
                            for pl in range(l + 1, l + numPlayers + 1):
                                players.append(stdoutLines[pl].strip())

                        # Add all read data to inst['stdout']
                        print('M:Inst {} stdout update after console command({} bytes)'.format(instName, len(stdoutRaw)))
                        inst['stdout'] = inst['stdout'] + stdout

                        # Return results
                        inst['action'] = ''
                        inst['lastResult'] = {'result': 'ok', 'players': players}

                # Only happens if the command is implemented in the command thread but not here
                else: inst['lastResult'] = {'result': 'err', 'reason': 'unknown command'}

                inst['action'] = ''

            # Average the time spent running the main loop and print it
            # if mainLoopNum % 100 == 0 and mainLoopNum > 0:
            #     avgLoopTime = sum(loopTimes) / len(loopTimes)
            #     loopTimes = []
            #     print('M:Loop {} (avg CPU time {}us)'.format(mainLoopNum, avgLoopTime / 1000))
            # mainLoopNum += 1

            # loopEndTime = perf_counter_ns()
            # loopTimes.append(loopEndTime - loopBeginTime)

            # 5 loops/second
            sleep(0.2)

        print('M:Exiting main loop')

    except:
        crashMsg = '\nM:Error in main loop:\n{}'.format(traceback.format_exc())
        print(crashMsg)
        with open(paths.factmand_crashes, 'a') as crashFile:
            crashFile.write(datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ': ' + crashMsg + '\n\n')

        # Stop all inst processes
        for instName in insts.keys():
            try: stopProc(instName); print('Stopped inst {}'.format(instName))
            except Exception as e:
                print('Failed to stop inst {}:\n{}'.format(instName, e.with_traceback()))

    finally:
        if flags.run:
            print('M:Setting flags.run to False')
            flags.run = False

        print('M:Joining command thread')
        commandThread.join()
        print('M:Main thread exiting')
