#!/bin/bash

if [ -z "$1" ]; then
    echo "Please provide a version"
    exit
fi

echo "Building release $1"
echo "Cleaning up previous builds..."

# Make sure the directories exist
mkdir releases/FactMan -p

# Remove any existing files
rm releases/FactMan/* -f
rm releases/FactMan-$1.7z -f

echo "Cleanup done"

# Copy files to release
cp commands.py  releases/FactMan/
cp factman.py   releases/FactMan/
cp factmand.py  releases/FactMan/
cp utils.py     releases/FactMan/
cp README.md    releases/FactMan/
cp LICENSE      releases/FactMan/

# Build the release file
cd releases
7z a FactMan-$1.7z FactMan
