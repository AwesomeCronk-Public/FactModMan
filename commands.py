#!/usr/bin/env python3

import requests
import json, shutil, subprocess, sys

from fileChecks import *
from utils import *


# TODO: Add reload-insts command and make all commands which may require a reload send the reload cmd


# General management commands

def cmd_check_install(args):
    results = {}

    # Check everything

    results['auth'] = checkIsFile(paths.auth)
    if results['auth'].ok: results['auth'] = checkAuth()

    results['config'] = checkIsFile(paths.config)
    if results['config'].ok: results['config'] = checkIsJSON(paths.config)
    # if results.config.ok: results.config = checkJSONStructure(paths.config, configFileStructures[paths.config])

    results['headless_cache'] = checkIsDir(paths.headless_cache)

    results['mod_cache'] = checkIsDir(paths.mod_cache)

    results['instances'] = checkIsDir(paths.instances)

    results['~/.local/bin'] = checkIsDir(paths.home.joinpath('.local/bin'))
    
    results['factmanLink'] = checkIsSymlink(paths.factmanLink, paths.factmanExec)

    results['headlessExec'] = checkIsFile(paths.headlessExec)
    if results['headlessExec'].ok: results['headlessExec'] = checkIsCallable(paths.headlessExec)

    for instDir in paths.instances.glob('*'):
        results['instances/' + instDir.name] = checkIsDir(instDir)

        resultKey = 'instances/' + instDir.name + '/config'
        results[resultKey] = checkIsDir(instDir.joinpath('config'))

        resultKey = 'instances/' + instDir.name + '/config/conig.ini'
        results[resultKey] = checkIsFile(instDir.joinpath('config/config.ini'))

        resultKey = 'instances/' + instDir.name + '/mods'
        results[resultKey] = checkIsDir(instDir.joinpath('mods'))

        resultKey = 'instances/' + instDir.name + '/mods/mod-list.json'
        results[resultKey] = checkIsFile(instDir.joinpath('mods/mod-list.json'))

        resultKey = 'instances/' + instDir.name + '/saves'
        results[resultKey] = checkIsDir(instDir.joinpath('saves'))

        resultKey = 'instances/' + instDir.name + '/factman.json'
        results[resultKey] = checkIsFile(instDir.joinpath('factman.json'))
        if results[resultKey].ok: results[resultKey] = checkJSONStructure(instDir.joinpath('factman.json'), fileJSONStructures['factman.json'])

        resultKey = 'instances/' + instDir.name + '/server-adminlist.json'
        results[resultKey] = checkIsFile(instDir.joinpath('server-adminlist.json'))
        if results[resultKey].ok: results[resultKey] = checkJSONStructure(instDir.joinpath('server-adminlist.json'), fileJSONStructures['server-adminlist.json'])

        resultKey = 'instances/' + instDir.name + '/server-banlist.json'
        results[resultKey] = checkIsFile(instDir.joinpath('server-banlist.json'))
        if results[resultKey].ok: results[resultKey] = checkJSONStructure(instDir.joinpath('server-banlist.json'), fileJSONStructures['server-banlist.json'])

        resultKey = 'instances/' + instDir.name + '/server-whitelist.json'
        results[resultKey] = checkIsFile(instDir.joinpath('server-whitelist.json'))
        if results[resultKey].ok: results[resultKey] = checkJSONStructure(instDir.joinpath('server-whitelist.json'), fileJSONStructures['server-whitelist.json'])

    # Report results
    for result in results.values():
        print(result)

    ok = True
    for result in results.values(): ok &= result.ok
    
    if ok: print('All is ok')
    else:
        # Prompt to fix issues or leave
        choice = inputChoice(('y','n'), 'Correct these issues? Files that aren\'t correctable will be replaced with default data.')
        if choice == 'n':
            print('No changes made')
            return

    # Fix issues
    for resultKey in results.keys():
        if not results[resultKey].ok: results[resultKey].fix()

def cmd_update_headless(args):
    if paths.headless.exists():
        print('Removing old headless')
        shutil.rmtree(paths.headless)
    
    version = fetchLatestHeadlessVersion()
    print('Updating to version {}'.format(version))
    downloadHeadlessToCache(version)
    extractHeadless(version)

def cmd_auth(args):
    username, token = getAuth(refresh=args.refresh)
    print('Username: {}\nToken: {}'.format(username, token))

def cmd_edit_file(args):
    configJSON = readConfigFile()

    if args.inst == 'factman':
        fileNames = {
            'config': 'config.json'
        }
    else:
        fileNames = {
            'whitelist': 'server-whitelist.json',
            'banlist':   'server-banlist.json',
            'adminlist': 'server-adminlist.json',
            'datafile':  'factman.json'
        }

    if not args.file in fileNames.keys():
        print('{} is not an editable file. Options are {}'.format(args.file, ', '.join(list(fileNames.keys()))))
        sys.exit(1)

    if args.inst == 'factman':
        filePath = paths.home.joinpath(fileNames[args.file])
    else:
        filePath = paths.instances.joinpath(args.inst, fileNames[args.file])

    subprocess.run([configJSON['editor'], *configJSON['editor-pre-args'], filePath, *configJSON['editor-post-args']])


# Instance commands

def cmd_list_insts(args):
    # List instances dir
    instsListing = loadInstsListing()
    
    if args.simple:
        print('\n'.join([instName for instName in instsListing.keys()]))

    else:
        table = {'Inst Name': [], 'Port': [], 'Automatic': [], 'Status': []}

        for instName in instsListing.keys():
            table['Inst Name'].append(instName)
            table['Port'].append(str(instsListing[instName]['port']))
            table['Automatic'].append(('no', 'yes')[instsListing[instName]['automatic']])

            resp = sendCmd({
                'cmd': 'status',
                'inst': instName
            }, exitOnFail=False)

            if resp['result'] == 'ok': table['Status'].append(resp['status'])
            else: table['Status'].append('err ({})'.format(resp['reason']))

        printTable(table)

def cmd_inst_info(args):
    if not args.n:
        instName = args.name
        
        # Automatic, port
        with open(paths.instances.joinpath(instName, 'factman.json'), 'r') as instDataFile:
            instDataJSON = json.load(instDataFile)
        print('Automatic: {}\nPort: {}'.format(instDataJSON['automatic'], instDataJSON['port']))
        
        # Headless status
        resp = sendCmd({
            'cmd': 'status',
            'inst': instName
        })
        if resp['result'] == 'ok': print('Status: {}'.format(resp['status']))
        else: print('Failed to get status {}'.format(json.dumps(resp)))

        # Crash count, time since last crash
        resp = sendCmd({
            'cmd': 'crashInfo',
            'inst': instName
        })
        if resp['result'] == 'ok': print('Crash count: {}\nLast crash: {}'.format(resp['count'], resp['timeOfLast']))
        else: print('Failed to get crash info ({})'.format(json.dumps(resp['reason'])))

    # Get online players
    resp = sendCmd({
        'cmd': 'players',
        'inst': instName
    })
    if resp['result'] == 'ok':
        line = '{} players'.format(len(resp['players']))
        if len(resp['players']):
            line = line + ': ' + ', '.join(resp['players'])
        print(line)
    else:
        print('Failed to get players list ({})'.format(json.dumps(resp['reason'])))

    if args.o:
        # Get headless stdout JSON length
        resp = sendCmd({
            'cmd': 'stdoutJSONLen',
            'inst': instName
        })
        if resp['result'] == 'ok':
            stdoutJSONLen = resp['stdout']  # TODO: Fix frontend crash if stdoutJSONLen cmd fails
        else: print('Failed to get stdout repr length ({})'.format(json.dumps(resp['reason'])))

        print('stdout encodes at {} chars long, recving up to {} bytes'.format(stdoutJSONLen, stdoutJSONLen + 100))

        # Get headless stdio content
        # JSON response length is 42 or 58 without stdio content, 100 leaves plenty extra
        resp = sendCmd({
            'cmd': 'stdout',
            'inst': instName
        }, recvLen=100 + stdoutJSONLen)
        stdout = resp['stdout']
        
    # Print headless stdout
    if args.o:
        lines = [line.strip() for line in stdout.split('\n')]
        if args.t > 0:
            linesToPrint = lines[-args.t:]
            print('Output lines {} through {}:'.format(len(lines) + 1 - args.t, len(lines)))
        else:
            linesToPrint = lines
            print('Full output:')

        print('\n'.join(linesToPrint))

    # Print headless stderr
    # if args.e:
    #     lines = [line.strip() for line in stderr.split('\n')]
    #     if args.t > 0:
    #         linesToPrint = lines[-t:]
    #         print('Output lines {} through {}:'.format(len(lines) + 1 - t, len(lines)))
    #     else:
    #         linesToPrint = lines
    #         print('Full output:')
    # 
    #     print('\n'.join(linesToPrint))

def cmd_add_inst(args):
    # Make instance dir
    newInstDir = paths.instances.joinpath(args.name)
    newInstDir.mkdir()

    # Make config dir
    newInstDir.joinpath('config').mkdir()

    # Make config/config.ini
    with open(newInstDir.joinpath('config', 'config.ini'), 'w') as configFile:
        configFile.write(
            defaultFileContents['config.ini'].format(
                readData=str(paths.headless.joinpath('data')),
                writeData=str(newInstDir)
            )
        )

    # Make mods dir
    newInstDir.joinpath('mods').mkdir()

    # Make mods/mod-list.json
    with open(newInstDir.joinpath('mods', 'mod-list.json'), 'w') as modlistFile:
        modlistFile.write(defaultFileContents['mod-list.json'])

    # Make saves dir
    newInstDir.joinpath('saves').mkdir()

    # Make factman.json
    with open(newInstDir.joinpath('factman.json'), 'w') as dataFile:
        dataFile.write(defaultFileContents['factman.json'])

    # Make server-whitelist.json
    with open(newInstDir.joinpath('server-whitelist.json'), 'w') as whitelistFile:
        whitelistFile.write(defaultFileContents['server-whitelist.json'])

    # Make server-banlist.json
    with open(newInstDir.joinpath('server-banlist.json'), 'w') as banlistFile:
        banlistFile.write(defaultFileContents['server-banlist.json'])

    # Make server-adminlist.json
    with open(newInstDir.joinpath('server-adminlist.json'), 'w') as adminlistFile:
        adminlistFile.write(defaultFileContents['server-adminlist.json'])

    # Inform the user that they need to provide a save file
    print('No save file created. Please create a save with the name "{}" and place its .zip file in {}'.format(args.name, newInstDir.joinpath('saves')))

    # The rest should be produced by the factorio binary when it first runs a save

    # Tell factmand to reload the insts list
    print('Issuing reload cmd to factmand')
    resp = sendCmd({
        'cmd': 'reload'
    })
    print(json.dumps(resp))

def cmd_rm_inst(args):
    choice = inputChoice(('y', 'n'), 'Confirm deletion of {}? ALL DATA WILL BE LOST!'.format(args.name))
    if choice == 'n':
        print('Canceled, no files changed')
        return
    
    # Remove the inst dir
    print('Removing {}'.format(args.name))
    try: shutil.rmtree(paths.instances.joinpath(args.name))
    except FileNotFoundError:
        print('Instance dir does not exist')

    # Tell factmand to reload the insts list
    print('Issuing reload cmd to factmand')
    resp = sendCmd({
        'cmd': 'reload'
    })
    print(json.dumps(resp))

# Print the path to the datafile
# Use like this to edit the datafile: vim $(factman get-inst-datafile YourInstName)
def cmd_get_inst_datafile(args):
    print(str(paths.instances.joinpath(args.name, 'factman.json')))

def cmd_start_inst(args):
    resp = sendCmd({
        'cmd': 'start',
        'inst': args.name
    })
    print(json.dumps(resp))

def cmd_autostart_insts(args):
    print('Autostarting insts')
    resp = sendCmd({
        'cmd': 'autostart'
    })
    
    if resp['result'] == 'err':
        if resp['reason'] == 'no insts to start':
            print('No insts needing started')
        else:
            print(resp)
    else:
        print(', '.join(resp['started']))

# TODO: Say something nice if getTargetInstNames returns []
def cmd_stop_inst(args):
    for instName in getTargetInstNames(args.name):
        print('Stopping {}...'.format(instName), end=' ')
        resp = sendCmd({
            'cmd': 'stop',
            'inst': instName
        })

        if resp['result'] == 'ok':
            print('OK')
        else:
            try: print('ERR', json.dumps(resp['reason']))
            except KeyError: print('ERR', json.dumps(resp))


# Mod commands

def cmd_list_mods(args):
    # TODO: Check that args.instance is valid
    instModList = readInstModList(args.instance)

    if args.simple:
        print('\n'.join(list(instModList.keys())))

    else:
        # Path.glob returns a generator, which can't be iterated twice
        fileList = list(sorted(paths.instances.joinpath(args.instance, 'mods').glob('*.zip')))
        table = {'Mod Name': [], 'Enabled': [], 'Version': [], 'Mod Page': []}

        for modName in instModList.keys():
            modEnabled = ('no', 'yes')[instModList[modName]]
            modVersion = getModVersionFromFileList(modName, fileList)
            if modVersion is None: modVersion = '(not installed)'
            if modName in builtinMods: modVersion = '(built in)'
            modPage = 'https://mods.factorio.com/mod/{}'.format(modName)

            table['Mod Name'].append(modName)
            table['Enabled'].append(modEnabled)
            table['Version'].append(modVersion)
            table['Mod Page'].append(modPage)
        
        printTable(table)

def cmd_mod_info(args):
    modName = args.name
    modPage = 'https://mods.factorio.com/mod/' + modName

    # Handle builtin mods
    if modName in builtinMods:
        print('{} is built-in'.format(modName))
        print('Mod page: {}'.format(modPage))
        return

    resp = requests.get('https://mods.factorio.com/api/mods/{}/full'.format(modName))
    jsonResp = resp.json()

    # There was a problem, most likely because the provided name was wrong
    if 'message' in jsonResp.keys():
        print(jsonResp['message'])
        return

    releases = jsonResp['releases']
    versions = [release['version'] for release in releases]

    if args.v == 'latest': targetVersion = releases[-1]['version']
    else: targetVersion = args.v

    if not targetVersion in versions:
        print('{} is not an available version'.format(targetVersion))
        return

    targetRelease = releases[versions.index(targetVersion)]

    print('"{}" ({}) by {}'.format(jsonResp['title'], modName, jsonResp['owner']))
    print(jsonResp['summary'])
    print('Versions: {}'.format(', '.join(versions)))
    print('Details about version {}:'.format(targetVersion))
    print('  Factorio version: {}'.format(targetRelease['info_json']['factorio_version']))
    print('  Dependencies:\n  - {}'.format('\n  - '.join(targetRelease['info_json']['dependencies'])))
    print('Mod page: {}'.format(modPage))

def cmd_add_mod(args):
    modName = args.name
    
    if not modName in builtinMods:
        try: targetRelease, targetVersion = fetchTargetModRelease(modName, args.v)
        except ModAPIError as error:
            print('Failed to fetch release for latest version. Check your spelling. If you know the version number and it is cached, try add-mod with the specific version.')
            print('Message: {}'.format(error.args[0]))
            return
        except ValueError as error:
            print('Yeah no clue what\'s up, this should never have happened. Here\'s a traceback, should you be so kind as to report this:')
            print(error.with_traceback())
            return

        # Download target version
        modFileName = '{}_{}.zip'.format(modName, targetVersion)
        if paths.mod_cache.joinpath(modFileName) in paths.mod_cache.glob('*.zip'):
            print('Already downloaded')
        else:
            downloadModToCache(modName, modFileName, targetRelease)

    instNames = getTargetInstNames(args.instance)
    for instName in instNames:
        if modName in builtinMods: print('Adding {} to {}'.format(modName, instName))
        else: print('Adding {} {} to {}'.format(modName, targetVersion, instName))

        instModList = readInstModList(instName)
        currentVersion = getModVersionFromFileList(modName, paths.instances.joinpath(instName, 'mods').glob('*.zip'))

        # Skip this inst if the mod is already in the modlist
        if modName in instModList.keys():
            if modName in builtinMods: print('{} is already in mod list.'.format(modName))
            else: print('{} is already in mod list. To install or update it, use update-mod.'.format(modName))
            continue

        # Copy mod file to inst
        if not modName in builtinMods:
            if currentVersion != None:
                print('{} is already installed. To update it, use update-mod.'.format(modName))
            else:
                shutil.copy(
                    paths.mod_cache.joinpath(modFileName),
                    paths.instances.joinpath(instName, 'mods', modFileName)
                )

        # Mark it as enabled in the mod list
        instModList[modName] = True
        writeInstModList(instName, instModList)
        print('Added {} to mod list'.format(modName))

def cmd_rm_mod(args):
    modName = args.name

    instNames = getTargetInstNames(args.instance)
    for instName in instNames:
        print('Removing {} from {}'.format(modName, instName))
        
        instModList = readInstModList(instName)
        currentVersion = getModVersionFromFileList(modName, paths.instances.joinpath(instName, 'mods').glob('*.zip'))

        if not modName in instModList.keys():
            print('{} is not in mod list'.format(modName))
        
        else:
            del instModList[modName]
            writeInstModList(instName, instModList)

        if not modName in builtinMods:
            if currentVersion is None:
                print('{} is not installed'.format(modName))

            else:
                paths.instances.joinpath(instName, 'mods', '{}_{}.zip'.format(modName, currentVersion)).unlink()
                print('Removed mod file')

def cmd_update_mod(args):
    modName = args.name
    instNames = getTargetInstNames(args.instance)

    # Get all mod names as a single list and by inst name
    if modName == wildcardName:
        modNamesByInstName = {}
        allModNames = []
        for instName in instNames:
            instModList = readInstModList(instName)
            modNamesByInstName[instName] = list(instModList.keys())

            for modName in modNamesByInstName[instName]:
                if (not modName in allModNames):
                    allModNames.append(modName)

            # Remove built in mods from these lists
            modNamesByInstName[instName] = [modName for modName in modNamesByInstName[instName] if not modName in builtinMods]
            allModNames                  = [modName for modName in allModNames                  if not modName in builtinMods]
                

    # Check all specified insts for the mod
    else:
        if modName in builtinMods:
            print('{} is built in and cannot be updated'.format(modName))
            return

        modNamesByInstName = {}
        allModNames = [modName]
        for instName in instNames:
            instModList = readInstModList(instName)

            # If the inst name wasn't a wildcard then no match is an error
            if args.instance != wildcardName and not modName in instModList:
                print('{} is not in {} mod list'.format(modName, instName))
                sys.exit(1)

            if modName in instModList:
                modNamesByInstName[instName] = [modName]
        
    targetVersions = {}
    modFileNames = {}

    for modName in allModNames:
        print('Fetching release for {} {}'.format(modName, args.v))
        # Fetch target release
        try: targetRelease, targetVersion = fetchTargetModRelease(modName, args.v)
        except ModAPIError as error:
            print('Failed to fetch release for {} {}'.format(modName, args.v))
            print('Message: {}'.format(error.args[0]))
            return

        # TODO: ValueError when an invalid version is specified, plus error.with_traceback() crashes
        except ValueError as error:
            print('Yeah no clue what\'s up, this should never have happened. Here\'s a traceback, should you be so kind as to report this:')
            print(error.with_traceback())
            return

        if args.v == 'latest':
            print('{} latest is {}'.format(modName, targetVersion))

        # Download target version
        modFileName = '{}_{}.zip'.format(modName, targetVersion)
        if paths.mod_cache.joinpath(modFileName) in paths.mod_cache.glob('*.zip'):
            print('Already downloaded')
        else:
            downloadModToCache(modName, modFileName, targetRelease)

        targetVersions[modName] = targetVersion
        modFileNames[modName] = modFileName

    # Update each specified mod in each specified instance
    for instName in instNames:
        for modName in modNamesByInstName[instName]:

            targetVersion = targetVersions[modName]
            currentVersion = getModVersionFromFileList(modName, paths.instances.joinpath(instName, 'mods').glob('*.zip'))

            if targetVersion == currentVersion:
                print('{} is already version {} in {}'.format(modName, targetVersion, instName))
                continue

            print('Updating {} to {} in {}'.format(modName, targetVersion, instName))

            # Remove current version
            if currentVersion != None:
                paths.instances.joinpath(instName, 'mods', '{}_{}.zip'.format(modName, currentVersion)).unlink()

            # Copy the new file
            shutil.copy(
                paths.mod_cache.joinpath(modFileNames[modName]),
                paths.instances.joinpath(instName, 'mods', modFileNames[modName])
            )
            # print('{} is now version {}'.format(modName, targetVersion))


def cmd_factmand_dbg(args):
    resp = sendCmd({
        'cmd': 'dbg'
    })
    print(json.dumps(resp))

# TODO: Make this more user friendly
def cmd_factmand_status(args):
    resp = sendCmd({
        'cmd': 'status'
    })
    print(json.dumps(resp))
