import json, sys

from commands import sendCmd


if __name__ == '__main__':
    try: target = sys.argv[1]
    except IndexError:
        print('Please specify cmd or main')
        sys.exit(1)
    
    if not target in ('cmd', 'main'):
        print('Can\'t trigger a crash in {}'.format(target))
        sys.exit(1)

    print('Sending crash{} to factmand'.format(target))
    resp = sendCmd({'cmd': 'crash{}'.format(target)})
    print(json.dumps(resp))
