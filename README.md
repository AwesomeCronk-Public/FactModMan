# FactMan

(Factorio Manager)

FactMan is my take on a server manager for Factorio. It can set up multiple server instances, install and update the headless server binary, and install and update mods.

All server instances will use the same binary and, as a result, must be the same version. When setting up a new installation, the latest version is used.

## Installation

FactMan is intended to have its own user set up prior to installation. This user is intended to be called `factorio`. All files will be stored in `/home/factorio`.

FactMan only supports Linux.

### Steps

TODO: Finish this

1. Create a user called `factorio` (Ensure it is in the `factorio` group and the home directory is created (`/home/factorio/`))
2. Switch to the `factorio` user. All further instructions assume that you are under the `factorio` user.
2. Navigate to https://gitlab.com/AwesomeCronk-Public/FactMan/-/releases and download the latest release of FactMan. Unpack the file to `/home/factorio/`.
    - Alternatively you can `git clone https://gitlab.com/AwesomeCronk-Public/FactMan` for the absolutely cutting edge latest stuff. Fair warning, there's usually a good chance this is broken!
3. Create a symlink to `/home/factorio/FactMan/factman.py` at `/home/factorio/.local/bin/factorio` (this will be handled by `check-install` eventually)
4. Run `factman check-install` to check what is missing or broken and, when prompted, reply `y` or `yes` to automatically set everything up.

Depending on how you created the factorio user you may need to create `/home/factorio/.bin/local` and add it to the path. On Debian 12 I found that I could just copy `.profile` from another user's home directory. Once both steps were done, logging out and back in should put it in the path. I found that just using `su` to get to the `factorio` user tended to keep the original user's path.

### File Structure

```
/home/factorio
|-FactMan
| `-(FactMan files)
|-factorio
| `-(server files)
|-instances
| |-SomeServerName
| `-SomeOtherServername
|-mod-cache
| `-(mod files)
|-headless-cache
| `-(headless install/upgrade files)
`-auth
```
