#!/bin/python3
import argparse, pathlib, sys

import json, requests

from commands import *

from utils import _version


def getArgs(argv):
    rootParser = argparse.ArgumentParser('factman', description='FactMan {}: Factorio Manager'.format(_version))

    subParsers = rootParser.add_subparsers()
    subParsers.required = True

    ### Setup/overall management commands ###

    parser_check_install = subParsers.add_parser('check-install', help='Check FactMan installation and environment')
    parser_check_install.set_defaults(function=cmd_check_install)

    parser_update_headless = subParsers.add_parser('update-headless', help='Update the headless server (recommended to stop all insts first)')
    parser_update_headless.set_defaults(function=cmd_update_headless)

    parser_auth = subParsers.add_parser('auth', help='Show or refresh auth data')
    parser_auth.set_defaults(function=cmd_auth)
    parser_auth.add_argument(
        '-r', '--refresh',
        action='store_true',
        help='Refresh auth data'
    )

    parser_edit_file = subParsers.add_parser('edit-file', help='Edit a file')
    parser_edit_file.set_defaults(function=cmd_edit_file)
    parser_edit_file.add_argument(
        'inst',
        type=str,
        help='Instance name (use "factman" to edit FactMan files)'
    )
    parser_edit_file.add_argument(
        'file',
        type=str,
        help='File to edit (not file path)'
    )
 
    ### Instance commands ###

    parser_list_insts = subParsers.add_parser('list-insts', help='List instances')
    parser_list_insts.set_defaults(function=cmd_list_insts)
    parser_list_insts.add_argument(
        '-s',
        '--simple',
        action='store_true',
        help='Print a simple listing instead of a table'
    )

    parser_inst_info = subParsers.add_parser('inst-info', help='Show instance info')
    parser_inst_info.set_defaults(function=cmd_inst_info)
    parser_inst_info.add_argument(
        'name',
        type=str,
        help='Instance name'
    )
    parser_inst_info.add_argument(
        '-o',
        action='store_true',
        help='Print the headless server\'s stdout/stderr'
    )
    parser_inst_info.add_argument(
        '-t',
        type=int,
        default=10,
        help='Truncate the headless server\'s stdout/stderr (default 10 lines, 0 or less to disable truncation)'
    )
    parser_inst_info.add_argument(
        '-n',
        action='store_true',
        help='Do not print the headless server\'s status'
    )

    parser_add_inst = subParsers.add_parser('add-inst', help='Add an instance')
    parser_add_inst.set_defaults(function=cmd_add_inst)
    parser_add_inst.add_argument(
        'name',
        type=str,
        help='Instance name'
    )

    parser_rm_inst = subParsers.add_parser('rm-inst', help='Remove an instance')
    parser_rm_inst.set_defaults(function=cmd_rm_inst)
    parser_rm_inst.add_argument(
        'name',
        type=str,
        help='Instance name'
    )

    parser_get_inst_datafile = subParsers.add_parser('get-inst-datafile', help='Add an instance')
    parser_get_inst_datafile.set_defaults(function=cmd_get_inst_datafile)
    parser_get_inst_datafile.add_argument(
        'name',
        type=str,
        help='Instance name'
    )

    parser_start_inst = subParsers.add_parser('start-inst', help='Start an instance')
    parser_start_inst.set_defaults(function=cmd_start_inst)
    parser_start_inst.add_argument(
        'name',
        type=str,
        help='Instance name'
    )

    parser_autostart_insts = subParsers.add_parser('autostart-insts', help='Automatically start all enabled instances')
    parser_autostart_insts.set_defaults(function=cmd_autostart_insts)

    parser_stop_inst = subParsers.add_parser('stop-inst', help='Stop an instance')
    parser_stop_inst.set_defaults(function=cmd_stop_inst)
    parser_stop_inst.add_argument(
        'name',
        type=str,
        help='Instance name'
    )

    ### Mod commands ###

    parser_list_mods = subParsers.add_parser('list-mods', help='List mods in an instance')
    parser_list_mods.set_defaults(function=cmd_list_mods)
    parser_list_mods.add_argument(
        'instance',
        type=str,
        help='Instance name'
    )
    parser_list_mods.add_argument(
        '-s',
        '--simple',
        action='store_true',
        help='Print a simple listing instead of a table'
    )

    parser_mod_info = subParsers.add_parser('mod-info', help='Get info about a mod')
    parser_mod_info.set_defaults(function=cmd_mod_info)
    parser_mod_info.add_argument(
        'name',
        type=str,
        help='Technical mod name'
    )
    parser_mod_info.add_argument(
        '-v',
        type=str,
        default='latest',
        help='Version to get details on'
    )

    parser_add_mod = subParsers.add_parser('add-mod', help='Add a mod to an instance')
    parser_add_mod.set_defaults(function=cmd_add_mod)
    parser_add_mod.add_argument(
        'instance',
        type=str,
        help='Instance name'
    )
    parser_add_mod.add_argument(
        'name',
        type=str,
        help='Technical mod name'
    )
    parser_add_mod.add_argument(
        '-v',
        type=str,
        default='latest',
        help='Version to add (default: latest)'
    )

    parser_rm_mod = subParsers.add_parser('rm-mod', help='Remove a mod from an instance')
    parser_rm_mod.set_defaults(function=cmd_rm_mod)
    parser_rm_mod.add_argument(
        'instance',
        type=str,
        help='Instance name'
    )
    parser_rm_mod.add_argument(
        'name',
        type=str,
        help='Technical mod name'
    )

    parser_update_mod = subParsers.add_parser('update-mod', help='Update a mod in an instance')
    parser_update_mod.set_defaults(function=cmd_update_mod)
    parser_update_mod.add_argument(
        'instance',
        type=str,
        help='Instance name'
    )
    parser_update_mod.add_argument(
        'name',
        type=str,
        help='Technical mod name'
    )
    parser_update_mod.add_argument(
        '-v',
        type=str,
        default='latest',
        help='Version to add (default: latest)'
    )

    ### factmand debugging commands (for development use, will be replaced) ###

    parser_factmand_dbg = subParsers.add_parser('factmand-dbg', help='Send the dbg command to factmand')
    parser_factmand_dbg.set_defaults(function=cmd_factmand_dbg)

    parser_factmand_status = subParsers.add_parser('factmand-status', help='Send the status command to factmand')
    parser_factmand_status.set_defaults(function=cmd_factmand_status)

    return rootParser.parse_args(argv)

if __name__ == '__main__':
    args = getArgs(sys.argv[1:])
    args.function(args)
