import grp, json, os, pathlib

from utils import *


class CheckResult:
    def __init__(self, path: str, ok: bool, reason: str):
        self.path = path
        self.ok = ok
        self.reason = reason
        self.fix = lambda: print('NO FIX SPECIFIED for {}'.format(self.path))

        if (not self.ok) and self.reason == '':
            raise ValueError('Must provide reason if not ok')

    def __repr__(self):
        if self.ok: return 'OK  {}'.format(self.path)
        else: return 'ERR {}: {}'.format(self.path, self.reason)

    def fix(self):
        raise NotImplementedError('CheckResult.fix')


def checkIsFile(path: pathlib.Path):
    if not path.exists():
        result = CheckResult(path, False, 'does not exist')
        result.fix = lambda: writeFileDefault(path)

    elif not path.is_file():
        result = CheckResult(path, False, 'is not a file')
        result.fix = lambda: deleteFileOrDir(); writeFileDefault(path)

    else: result = CheckResult(path, True, '')

    return result

def checkIsDir(path: pathlib.Path):
    if not path.exists():
        result = CheckResult(path, False, 'does not exist')
        result.fix = lambda: path.mkdir(parents=True)

    elif not path.is_dir():
        result = CheckResult(path, False, 'is not a directory')
        result.fix = lambda: deleteFileOrDir(); path.mkdir(parents=True)

    else: result = CheckResult(path, True, '')

    return result

def checkIsSymlink(path: pathlib.Path, targetPath: pathlib.Path):
    if not path.exists():
        result = CheckResult(path, False, 'does not exist')
        result.fix = lambda: print('Please create symlink to {} at {}'.format(targetPath, path))

    elif not path.is_symlink():
        result = CheckResult(path, False, 'is not a symlink')
        result.fix = lambda: print('Please remove the item at {} and create symlink to {}'.format(path, targetPath))

    else: result = CheckResult(path, True, '')

    return result

# Check that the user running FactMan can call the provided executable
def checkIsCallable(path: pathlib.Path):
    st_mode = path.stat().st_mode
    ownerCallable = bool(st_mode & 0o100)
    groupCallable = bool(st_mode & 0o010)
    otherCallable = bool(st_mode & 0o001)

    currentUser = os.getlogin()
    currentGroups = [g.gr_name for g in grp.getgrall() if currentUser in g.gr_mem]

    if (ownerCallable and path.owner() == currentUser) \
    or (groupCallable and path.group() in currentGroups) \
    or otherCallable:
        result = CheckResult(path, True, '')

    else:
        result = CheckResult(path, False, 'current user no execute permission')

        if currentUser == 'factorio':
            # chown factorio:factorio path
            result.fix = lambda: os.chown(path, os.getuid(), grp.getgrnam('factorio'))
        else:
            result.fix = lambda: print('Please ensure the factorio user has access to {}'.format(path))

    return result

# Check that the auth file is valid
def checkAuth():
    try: getAuth(refresh=False)

    except AuthError:
        result = CheckResult(paths.auth, False, 'auth file invalid')
        result.fix = lambda: print('Please run factman auth')

    except Exception as e:
        result = CheckResult(paths.auth, False, 'failed to get auth ({})'.format(e))
        result.fix = lambda: print('Seek professional help')

    else: result = CheckResult(paths.auth, True, '')

    return result

# Check that the given file is valid JSON
def checkIsJSON(path: pathlib.Path):
    with path.open('r') as file:
        try: json.load(file)

        except UnicodeDecodeError or json.JSONDecodeError:
            result = CheckResult(path, False, 'file is not JSON')
            result.fix = lambda: print('Please fix {}'.format(path))

        except Exception as e:
            result = CheckResult(path, False, 'failed to check file ({})'.format(e))
            result.fix = lambda: print('Seek professional help')

        else:
            result = CheckResult(path, True, '')

        return result

# TODO: Finish
def checkJSONStructure(path: pathlib.Path, structure):
    # JSON data types are str, int, float, dict, list, bool, None
    # Current ideas: 
    # - Provide a JSON structure but for things whose value we don't care, only the type, pass a
    #   Python type
    # - For dicts, check that the key/values match the structure, ignore extra keys that aren't
    #   matched by a type
    # - For lists, check that the values match the structure, ignore extra values
    # - For tuples, treat same as lists but flag on extra values
    # - For dicts, when structure key is a type, check all key/values in JSON with that entry,
    #   unless they match a structure key that is a value

    def _checkObjStructure(obj, structure):
        result = CheckResult(path, True, '')
        structureType = type(structure)

        # When structure is a type we only care that obj is of the same type
        if structureType is type:
            if not isinstance(obj, structure):
                result = CheckResult(path, False, 'Incorrect type {}, expected {}'.format(type(obj), structure))
                result.fix = lambda: print('Please fix {}'.format(path))

        # When structure is an instance we care what its value is
        else:
            if not (isinstance(obj, structureType) or (type(obj) is list and structureType is tuple)):
                result = CheckResult(path, False, 'Incorrect type {}, expected {}'.format(type(obj), structure))
                result.fix = lambda: print('Please fix {}'.format(path))

            # Simple values, just check if unequal
            elif structureType in (str, int, float, bool):
                if obj != structure:
                    result = CheckResult(path, False, 'Incorrect value {}, expected {}'.format(repr(obj), structure))
                    result.fix = lambda: print('Please fix {}'.format(path))

            # List of set length
            elif structureType is tuple:
                # Length must match
                if len(obj) != len(structure):
                    result = CheckResult(path, False, '{} values {}, expected {}'.format(('Too many' if len(obj) > len(structure) else 'Not enough'), len(obj), len(structure)))
                    result.fix = lambda: print('Please fix {}'.format(path))

                else:
                    for i, item in enumerate(obj):
                        # Dicts are special
                        if isinstance(item, dict):
                            subResult = _checkObjStructure(item, structure[i])
                            if not subResult.ok:
                                result = subResult
                                break
                        
                        elif not (item == structure[i] or type(item) == structure[i]):
                            result = CheckResult(path, False, 'Incorrect item {} in {}, does not match structure {}'.format(repr(item), obj, structure))
                            result.fix = lambda: print('Please fix {}'.format(path))
                            break

            # List of any length
            elif structureType is list:
                for item in obj:
                    # Dicts are special
                    if isinstance(item, dict):

                        # Search structure for dicts and _checkStructure against each one
                        for structureItem in structure:
                            if isinstance(structureItem, dict):
                                subResult = _checkObjStructure(item, structureItem)
                                if subResult.ok: break

                            # Accept dict type as wildcard, as usual
                            elif structureItem is dict: break
                        
                        # Nothing was ok
                        else:
                            result = CheckResult(path, False, 'Incorrect item {} in {}, does not match any dict structure option'.format(repr(item), obj))
                            result.fix = lambda: print('Please fix {}'.format(path))
                            break

                    # Other items are simpler to check
                    elif not (item in structure or type(item) in structure):
                        result = CheckResult(path, False, 'Incorrect item {} in {}, does not match structure {}'.format(repr(item), obj, structure))
                        result.fix = lambda: print('Please fix {}'.format(path))
                        break

            # Dict
            elif structureType is dict:
                for key in obj.keys():

                    if key in structure.keys():
                        subResult = _checkObjStructure(obj[key], structure[key])

                    elif type(key) in structure.keys():
                        subResult = _checkObjStructure(obj[key], structure[type(key)])
                    
                    else:
                        result = CheckResult(path, False, 'Incorrect key {}, does not match structure keys {}'.format(repr(key), list(structure.keys())))
                        result.fix = lambda: print('Please fix {}'.format(path))
                        break
                    
                    if not subResult.ok:
                        result = subResult
                        break

                for key in structure.keys():
                    if (not key in obj.keys()) and (not isinstance(key, type)):
                        result = CheckResult(path, False, 'Missing key {}'.format(key))
                        result.fix = lambda: print('Please fix {}'.format(path))
            
            else: raise NotImplementedError('Structure type {} is not implemented, please raise an issue.'.format(structureType))

        return result
    
    with open(path, 'r') as file:
        jsonObj = json.load(file)

    return _checkObjStructure(jsonObj, structure)


# For testing purposes
if __name__ == '__main__':
    structure = {
        'editor': str,
        'editor-pre-args': [str],
        'editor-post-args': [str]
    }
    results = checkJSONStructure('test.json', structure)
    print(results)
    if not results.ok: results.fix()
