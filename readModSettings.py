import struct, sys


filePath = sys.argv[1]

def parseModSettings(raw):
    versionMain = int.from_bytes(raw[0:2], 'little')
    versionMajor = int.from_bytes(raw[2:4], 'little')
    versionMinor = int.from_bytes(raw[4:6], 'little')
    versionPatch = int.from_bytes(raw[6:8], 'little')
    versionBranch = raw[8]

    print('{}.{}.{}-{} branch {}'.format(versionMain, versionMajor, versionMinor, versionPatch, versionBranch))
    if versionMain != 2 or versionMajor != 0:
        print('WARNING: Not tested on any version other than 2.0')

    return (versionMain, versionMajor, versionMinor, versionPatch, versionBranch, parsePTree(raw[9:]))

def parsePTree(raw, indent = ''):
    print(indent + 'Parsing property tree', raw[0:16].hex().upper())
    treeType = raw[0]
    # isAnyType = raw[1]
    
    if treeType == 0x00:
        print(indent + 'Is None')
        return None, raw[2:]
    
    elif treeType == 0x01:
        print(indent + 'Is bool', end=' ')
        value = raw[2] != 0x00
        print(indent + str(value))
        return value, raw[3:]
    
    elif treeType == 0x02:
        print(indent + 'Is float', end=' ')
        value = struct.unpack('<d', raw[2:10])
        print(indent + str(value))
        return value, raw[10:]
    
    elif treeType == 0x03:
        print(indent + 'Is str', end=' ')
        value, raw = parsePTreeString(raw[2:])
        print(indent + str(value))
        return value, raw
    
    elif treeType == 0x04:
        print(indent + 'Is list', end=' ')
        count = int.from_bytes(raw[2:6], 'little')
        print(indent + str(count))
        treeList = []
        for i in range(count):
            # Wube chose to include the dict key in list entries, discard it
            key, raw = parsePTreeString(raw[6:], indent + '  ')
            value, raw = parsePTree(raw, indent + '  ')
            treeList.append(value)
        print(indent + 'Done parsing list')
        return treeList, raw
    
    elif treeType == 0x05:
        print(indent + 'Is dict', end=' ')
        count = int.from_bytes(raw[2:6], 'little')
        print(indent + str(count))
        treeDict = {}
        for i in range(count):
            key, raw = parsePTreeString(raw[6:], indent + '  ')
            value, raw = parsePTree(raw, indent + '  ')
            # TODO: Check for duplicate keys
            treeDict[key] = value
        print(indent + 'Done parsing dict')
        return treeDict, raw
    
    elif treeType == 0x06:
        print(indent + 'Is int', end=' ')
        value = int.from_bytes(raw[2:10], 'little', signed=True)
        print(indent + str(value))
        return value, raw[10:]
    
    elif treeType == 0x07:
        print(indent + 'Is uint', end=' ')
        value = int.from_bytes(raw[2:10], 'little')
        print(indent + str(value))
        return value, raw[10:]

    print(indent + 'Failed to identify treeType: 0x{:02X}'.format(treeType))
    print(indent + 'Next 32 bytes of raw:\n{}'.format(raw[2:34]))

def parsePTreeString(raw, indent = ''):
    print(indent + 'Parsing property tree string', raw[0:16].hex().upper())
    empty = raw[0]
    if empty != 0x00:
        print(indent + 'Is empty')
        return '', raw[1:]

    size, raw = parsePackedUint_8_32(raw[1:], indent + '  ')
    value = raw[0:size].decode()
    print(indent + 'Is {} long "{}"'.format(size, value))
    return value, raw[size:]

def parsePackedUint_8_32(raw, indent = ''):
    print(indent + 'Parsing packeduint_8_32', raw[0:16].hex().upper())
    value = raw[0]
    if value == 0xff:
        value = int.from_bytes(raw[1:5], 'little')
        print(indent + 'Is', value)
        return value, raw[5:]
    print(indent + 'Is', value)
    return value, raw[1:]

if __name__ == '__main__':
    with open(filePath, 'rb') as file:
        print(parseModSettings(file.read()))
